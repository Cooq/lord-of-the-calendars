$LOAD_PATH << './lib'
$LOAD_PATH << '.'
require 'common/cli_parser'
require 'common/environment'
require 'common/log'
require 'datasources/source'
require 'icalendar'
require 'sinatra'
Dir["lib/datasources/*.rb"].each {|file| require file }

# Configure Calendar MIME Type
configure do
  mime_type :calendar, 'text/calendar'
end

def get_calendar endpoint
  calendar = Icalendar::Calendar.new
  
  # Get Calendar from sources
  Settings.EndPoints[endpoint].each do |source_settings|
    source = Object::const_get("LOTC::#{source_settings.class_name}").new source_settings.data
    source.fetch calendar
  end

  # Write file. DEBUG
  if Settings.Export.write_file
    Log.info "Writing calendar for #{endpoint}..."
    File.write("#{Settings.Export.filename}-#{endpoint}.ics", calendar.to_ical)
  end

  calendar
end

# Add the sinatra endpoints
Settings.EndPoints.each do |endpoint, sources|
  Log.info "Starting endpoint #{endpoint}..."
  get "/#{endpoint}" do
    Log.info "GET Calendar for /#{endpoint}"
    calendar = get_calendar endpoint
    content_type :calendar
    calendar.to_ical
  end
end

set :bind, '0.0.0.0'
set :port, Settings.Export.port