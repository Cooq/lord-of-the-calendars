require 'notion'
# require 'config'

module LOTC
  class NotionSource < LOTC::Source
    def initialize data
      # Start Notion API
      Notion.configure do |config|
        config.token = data.token
      end
      @client = Notion::Client.new
      @database_id = data.database_id # Available in the URL of notion.so
      @properties = data.properties
    end

    def fetch calendar
      # Used to not spam one hour with 10 events
      dates = {}

      # Default event duration and start time
      default_duration = Time.parse(@properties.default_duration||"1h00").seconds_since_midnight.seconds
      start_time = Time.parse(@properties.start_time||"9h00").seconds_since_midnight.seconds

      # Get event from the week and after
      filter = {
        'or': [
          {
            'property': @properties.start,
            'date': {
              'past_week': {}
            }
          },{
            'property': @properties.start,
            'date': {
              'next_year': {}
            }
          },
        ]
      }

      # Query Notion only 100 events
      @client.database_query(database_id: @database_id, filter: filter) do |page|
        page["results"].each do |res|
          # Parse start date. Default date is today
          dtstart = DateTime.now.midnight
          begin
            dtstart = DateTime.parse(res["properties"][@properties.start]["date"]["start"]) 
          rescue => exception
            Log.debug("could not parse start date for #{res["properties"]}")
          end
          dtstart += start_time
          dates_key = dtstart.dup
          # Check date start to not spam
          dtstart = dates[dates_key] if dates.key? dtstart

          # Parse duration. Default duration is default_duration
          duration = default_duration
          begin
            duration = Time.parse(res["properties"][@properties.duration]["rich_text"][0]["plain_text"]).seconds_since_midnight.seconds
          rescue => exception
            Log.debug("could not parse duration for #{res["properties"]}")
          end

          # Parse end date. default end is date start + duration
          dtend = dtstart.dup
          dtend += duration
          begin
            dtend = DateTime.parse(res["properties"][@properties.end]["date"]["start"])
          rescue => exception
            Log.debug("could not parse end date for #{res["properties"]}")
          end

          # Store last end for the day
          dates[dates_key] = dtend

          # Create event and store in calendar
          calendar.event do |e|
            e.dtstart     = dtstart
            e.dtend       = dtend
            e.summary     = res["properties"][@properties.name]["title"][0]["plain_text"]
            e.uid         = res["id"]
            e.description = res["properties"][@properties.description]["title"][0]["plain_text"]
            # e.ip_class    = "PRIVATE"
          end
        end
      end
    end
  end
end

