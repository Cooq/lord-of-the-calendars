module LOTC
  class Source
    FORMAT = '%Y%m%dT%H%M%S'
    def initialize data; end
    def fetch calendar; end
  end

  ## Exceptions
  
  module Exception
    class SourceNotDefined < StandardError; end
  end
end

