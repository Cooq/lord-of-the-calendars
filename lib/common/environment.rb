require 'notion'
require 'config'

Config.load_and_set_settings(Config.setting_files("config", Options[:environment]))

Settings.Export.port = Options[:port] if !Options[:port].nil?
