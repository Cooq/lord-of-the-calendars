require 'optparse'

Options = {}

OptionParser.new do |opts|
  opts.banner = "Usage: lotc.rb [options]"

  Options[:environment] = ENV['RUBY_ENV'] || "development"
  Options[:port] = ENV['PORT']

  opts.on("-eNAME", "--environment=NAME", "Force Environment") do |n|
    Options[:environment] = n
  end

  opts.on("-pPORT", "--port=PORT", "Force Port") do |n|
    Options[:port] = n
  end
end.parse!
