require 'common/environment'
require 'logger'
require 'time'

logging = Settings.Logging
path = logging.dig("path")

if !path.nil?
  Log = Logger.new(path, logging.max_number, logging.rotate_size)
else
  Log = Logger.new(STDOUT)
end

Log.formatter = proc do |severity, datetime, progname, msg|
  fileLine = "";
  caller.each do |clr|
      unless(/\/logger.rb:/ =~ clr || /\/logging.rb:/ =~ clr)
          fileLine = clr;
          break;
      end
  end
  fileLine = fileLine.sub(/^.+\/(.+):(\d+):in `(?:.+ )?([^ ]+)'/, '\1:\2|\3')
  res = fileLine.split "|"
  res << "" if res.size == 1
  begin
    "#{datetime.strftime("%F %T.%L")} %-5.5s %-13.13s %-13.13s #{msg}\n" % [severity, res[0], res[1]]
  rescue => exception
    "#{datetime} #{msg}"
  end
end

Log.level = logging.level
Log.debug "Starting Logging <level: #{Log.level}>"